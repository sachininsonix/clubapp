package com.clubapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by insonix on 12/5/16.
 */
public class HomeActivity extends Activity {
    LinearLayout sports,drink,swim,member,facility,contact;
    TextView textsports,textdrink,textswim,textmember,textfacility,textcontact;
    SlidingPaneLayout slidingPaneLayout;
    ImageView menu,homeepic;
    TextView hometext;
    ListView listView,mainlistview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homeactivity);
        slidingPaneLayout = (SlidingPaneLayout) findViewById(R.id.SlidingPanel);
        slidingPaneLayout.setSliderFadeColor(Color.TRANSPARENT);
        sports=(LinearLayout)findViewById(R.id.sports);
        drink=(LinearLayout)findViewById(R.id.drinks);
        swim=(LinearLayout)findViewById(R.id.swim);
        member=(LinearLayout)findViewById(R.id.member);
        facility=(LinearLayout)findViewById(R.id.facility);
        contact=(LinearLayout)findViewById(R.id.contact);
        mainlistview=(ListView)findViewById(R.id.MainMenuList);
        menu=(ImageView)findViewById(R.id.menu);
        homeepic=(ImageView)findViewById(R.id.homeepic);
        hometext=(TextView)findViewById(R.id.homeview);
        homeepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this,HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        hometext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this,HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingPaneLayout.openPane();
            }
        });
        mainlistview.setAdapter(new MenuAdapter(HomeActivity.this));
        sports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, Sports.class);
                startActivity(intent);
                finish();
            }
        });
        drink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, DrinkDine.class);
                startActivity(intent);
                finish();
            }
        });
        swim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeActivity.this,SwimmingPool.class);
                startActivity(intent);
                finish();
            }
        });
        member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, Login.class);
                startActivity(intent);
                finish();
            }
        });
        facility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeActivity.this,Clubfacilities.class);
                startActivity(intent);
                finish();
            }
        });
        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ContactUs.class);
                startActivity(intent);
                finish();
            }
        });


    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
