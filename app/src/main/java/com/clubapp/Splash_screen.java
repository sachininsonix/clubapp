package com.clubapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.clubapp.HomeActivity;
import com.clubapp.Login;
import com.clubapp.R;


/**
 * Created by mohit on 22-Feb-16.
 */
public class Splash_screen extends Activity  {
    /** Duration of wait **/
    ImageView tvMuvText;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
   setContentView(R.layout.splash);
        if ((getResources().getConfiguration().screenLayout &      Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {

            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        else if ((getResources().getConfiguration().screenLayout &      Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {

            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        preferences= PreferenceManager.getDefaultSharedPreferences(Splash_screen.this);
//        gpsTracker = new GPSTracker(Splash_screen.this);
        if(preferences.getString("key","").equals("")) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (  ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.VIBRATE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.INTERNET)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.ACCESS_WIFI_STATE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.ACCESS_NETWORK_STATE)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Splash_screen.this, android.Manifest.permission.WAKE_LOCK)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Splash_screen.this,
                            new String[]{
                                    android.Manifest.permission.VIBRATE, android.Manifest.permission.INTERNET,
                                    android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION,
                                    android.Manifest.permission.ACCESS_WIFI_STATE, android.Manifest.permission.ACCESS_NETWORK_STATE, android.Manifest.permission.WAKE_LOCK
                            },
                            1);
                }
            }else {
            }
        }
//        final NetworkInfo activeNetwork;
//        ConnectivityManager cm =
//                (ConnectivityManager) Splash_screen.this.getSystemService(Context.CONNECTIVITY_SERVICE);
//        activeNetwork = cm.getActiveNetworkInfo();
//        final boolean isConnected = activeNetwork != null &&
//                activeNetwork.isConnectedOrConnecting();
//
//
//        if (isConnected == true) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
//                    if (isNetworkAvailable() == false) {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(Splash_screen.this);
//                        builder.setMessage("Error in Network Connection")
//                                .setIcon(R.drawable.alert)
//                                .setCancelable(false)
//                                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        //  Action for 'NO' Button
//                                        dialog.cancel();
//                                    }
//                                });
//
//                        AlertDialog alert = builder.create();
//                        //Setting the title manually
//                        alert.setTitle("Alert");
//                        alert.show();
//                        //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
//                    }
//                    else
//                    if(preferences.getString("key","").equals("")) {
//                        Intent i = new Intent(Splash_screen.this, Login.class);
//                        startActivity(i);
//                        //Remove activity
//                        finish();
//                    }else{
                        Intent i = new Intent(Splash_screen.this, HomeActivity.class);
                        startActivity(i);
                        //Remove activity
                        finish();
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
//        }

//    else {
//            Toast.makeText(Splash_screen.this, " No Internet connection ", Toast.LENGTH_LONG).show();
//        }
    }
//    private boolean isNetworkAvailable() {
//        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//    }
    @Override protected void onResume()
    {
        super.onResume();
//        checkPlayServices();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
