package com.clubapp;

/**
 * Created by insonix on 22/2/16.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class ContactUs extends Activity {

    private ImageView imageViewDot,back;
    private ViewPager viewPager;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
TextView maintextcategory;

    Button getstarted;

    private int[] mImages = new int[] { R.drawable.bg, R.drawable.sportspager,
            R.drawable.drinking, R.drawable.entertainment, R.drawable.swimmingpool };

    private int[] mImagesDot = new int[] { R.drawable.scroll_dot1,
            R.drawable.scroll_dot2, R.drawable.scroll_dot3,

            R.drawable.scroll_dot4, R.drawable.scroll_dot5 };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.contactus);
        maintextcategory=(TextView)findViewById(R.id.maintextcategory);
        maintextcategory.setText("Contact Us");
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor=preferences.edit();
//        imageViewDot = (ImageView) this.findViewById(R.id.imageView_dot);
        back = (ImageView) this.findViewById(R.id.back);

//        viewPager = (ViewPager) this.findViewById(R.id.view_pager);

//        viewPager.setAdapter(adapter);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactUs.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });






        }



    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent i = new Intent(ContactUs.this, HomeActivity.class);
        startActivity(i);
        finish();
        overridePendingTransition(0, 0);
            finish();
    }
}