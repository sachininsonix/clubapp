package com.clubapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by insonix on 12/5/16.
 */
public class Clubfacilities extends Activity {
    ListView clubfaciliteslist;
    List<String> li;
    SlidingPaneLayout slidingPaneLayout;
    ImageView menu,homeepic;
    TextView hometext;
    ListView listView,mainlistview;
    private static String[] MOBILE_MODELS = {"Food World","Cafe Coffee Day","Games Room","Massage & Spa centre "
            ,"LifeStyle Centre", "Gymnasium", "Bukit theatrette ","Cold Storage","Windsor Shop","Childer Facilities","Library"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.clubfacilities);
        slidingPaneLayout = (SlidingPaneLayout) findViewById(R.id.SlidingPanel);
        slidingPaneLayout.setSliderFadeColor(Color.TRANSPARENT);
        clubfaciliteslist=(ListView)findViewById(R.id.clubfacilites);
        mainlistview=(ListView)findViewById(R.id.MainMenuList);
        menu=(ImageView)findViewById(R.id.menu);
        homeepic=(ImageView)findViewById(R.id.homeepic);
        hometext=(TextView)findViewById(R.id.homeview);
        homeepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Clubfacilities.this, HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        hometext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Clubfacilities.this, HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingPaneLayout.openPane();
            }
        });
        mainlistview.setAdapter(new MenuAdapter(Clubfacilities.this));
        ArrayAdapter arrayAdapter=new ArrayAdapter(Clubfacilities.this,R.layout.clubfaclitylist,R.id.faciltytext,MOBILE_MODELS);
        clubfaciliteslist.setAdapter(arrayAdapter);

    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View mainView = inflater.inflate(R.layout.clubfaclitylist, container, false);
//        ListView listView = (ListView) mainView.findViewById(R.id.clubfacilites);
//
//        listView.setAdapter(new ArrayAdapter<String>(Clubfacilities.this, android.R.layout.simple_list_item_1, MOBILE_MODELS));
//        return mainView;
//    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent=new Intent(Clubfacilities.this,HomeActivity.class);
        startActivity(intent);
        finish();
    }
}

