package com.clubapp;

/**
 * Created by insonix on 22/2/16.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class DrinkDine extends Activity {

    private ImageView imageViewDot,back;
    private ViewPager viewPager;
    private ImagePagerAdapter adapter;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    TextView maintextcategory;

    Button getstarted;

    private int[] mImages = new int[] { R.drawable.drinking, R.drawable.sportspager,
            R.drawable.bg, R.drawable.entertainment, R.drawable.swimmingpool };

    private int[] mImagesDot = new int[] { R.drawable.scroll_dot1,
            R.drawable.scroll_dot2, R.drawable.scroll_dot3,

            R.drawable.scroll_dot4, R.drawable.scroll_dot5 };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.drinkanddine);
        maintextcategory=(TextView)findViewById(R.id.maintextcategory);
        maintextcategory.setText("Drink & Drive");
        preferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor=preferences.edit();
        imageViewDot = (ImageView) this.findViewById(R.id.imageView_dot);
        back = (ImageView) this.findViewById(R.id.back);

        viewPager = (ViewPager) this.findViewById(R.id.view_pager);
        adapter = new ImagePagerAdapter();
        viewPager.setAdapter(adapter);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DrinkDine.this,HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        viewPager.setOnPageChangeListener(new OnPageChangeListener() {

            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub

            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub
                imageViewDot.setImageResource(mImagesDot[arg0]);
            }

            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });



        }

        private class ImagePagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mImages.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((ImageView) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Context context = DrinkDine.this;

            ImageView imageView = new ImageView(context);

//            int padding = context.getResources().getDimensionPixelSize(
//                    R.dimen.padding_medium);
//            imageView.setPadding(padding, padding, padding, padding);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            imageView.setImageResource(mImages[position]);

            ((ViewPager) container).addView(imageView, 0);

            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((ImageView) object);

        }

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent i = new Intent(DrinkDine.this, HomeActivity.class);
        startActivity(i);
        finish();
        overridePendingTransition(0, 0);

    }
}