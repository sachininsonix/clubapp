package com.clubapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.net.URL;
import java.util.HashMap;

/**
 * Created by insonix on 26/4/16.
 */
public class Login extends Activity {
    Button login_button;
    String url;
    TextView forgot_password;
    public static EditText etUserName, etPassword,etName;
    public static TextView register1;

    SharedPreferences preferences;
    NodeList nodelist,nodelevel;
    SharedPreferences.Editor editor;
    ProgressDialog pDialog;
    Document xmlFeed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        if ((getResources().getConfiguration().screenLayout &      Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {

            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        else if ((getResources().getConfiguration().screenLayout &      Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {

            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        preferences= PreferenceManager.getDefaultSharedPreferences(Login.this);
        editor=preferences.edit();
        etUserName=(EditText)findViewById(R.id.editUsername);
        etPassword=(EditText)findViewById(R.id.editPasswd);
        login_button=(Button)findViewById(R.id.login_button);


        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (isNetworkAvailable() == false) {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
//                    builder.setMessage("Error in Network Connection")
//                            .setIcon(R.drawable.alert)
//                            .setCancelable(false)
//                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    //  Action for 'NO' Button
//                                    dialog.cancel();
//                                }
//                            });
//
//                    AlertDialog alert = builder.create();
//                    //Setting the title manually
//                    alert.setTitle("Alert");
//                    alert.show();
//                    //Toast.makeText(Register.this,"Not Connected",Toast.LENGTH_LONG).show();
//                }
//                else if(etName.getText().toString().length() == 0){
//                    inputLayoutName.setError(getString(R.string.err_msg_name));
//                    etName.requestFocus();
//                }

//                else
                if(etUserName.getText().toString().length() == 0){
                    etUserName.setError("User Name is required!");
                    etUserName.requestFocus();
                }
//                else if(!emailAddress.getText().toString().matches(emailPattern)){
//                    emailAddress.setError("Invaild email!");
//                }
                else if(etPassword.getText().toString().length() == 0){
                    etPassword.setError("Password is required!");
                    etPassword.requestFocus();
                }
                else{
                    Intent i = new Intent(Login.this, HomeActivity.class);
                startActivity(i);
                finish();
                overridePendingTransition(0, 0);
                }
            }
        });
//        forgot_password.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(Login.this, ForgotPassword.class);
//                startActivity(i);
//                finish();
//                overridePendingTransition(0, 0);
//            }
//        });

    }
//    private boolean isNetworkAvailable() {
//        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//    }

//
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        finish();
        Intent i = new Intent(Login.this, HomeActivity.class);
        startActivity(i);
        finish();
        overridePendingTransition(0, 0);
    }
}
