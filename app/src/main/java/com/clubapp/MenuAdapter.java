package com.clubapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
//
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

/**
 * Created by online chd on 7/27/2015.
 */
class MenuAdapter extends BaseAdapter {
    Activity activity;
    SharedPreferences preferences;
    ProgressDialog progressDialog;
    String url,user,pass;
    SharedPreferences.Editor editor;
//    int[] image={R.drawable.product,R.drawable.setting,R.drawable.logout};
    String [] Menu1 = {"Products","Settings","Logout"};
    public MenuAdapter(Activity activity){
        this.activity=activity;
        preferences= PreferenceManager.getDefaultSharedPreferences(activity);
        editor=preferences.edit();
        progressDialog=new ProgressDialog(activity);

    }
    @Override
    public int getCount() {
        return Menu1.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView( final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView=inflater.inflate(R.layout.mainmenu,null);
//        ImageView menuimage=(ImageView)convertView.findViewById(R.id.menuimage);
        TextView item=(TextView)convertView.findViewById(R.id.item);
  //    final  TextView stat=(TextView)convertView.findViewById(R.id.stat);

        item.setText(Menu1[position]);
//        menuimage.setBackgroundResource(image[position]);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
//                    Intent intent=new Intent(activity,Product.class);
//                    activity.startActivity(intent);
                //    stat.setVisibility(View.VISIBLE);
//                    Intent intent=new Intent(activity,RouteActivity.class);
//
//                    activity.startActivity(intent);
//                    activity.overridePendingTransition(0, 0);
//                    activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

//                    activity.finish();
                } else if (position == 1) {
//                    Intent intent=new Intent(activity,Setting.class);
//                  activity.startActivity(intent);
                  //  stat.setVisibility(View.INVISIBLE);
//                    Intent intent=new Intent(activity,EditProfileActivity.class);
//                    activity.startActivity(intent);
                   
//                    activity.finish();
                } else if (position == 2) {
//                    logout();
                    //stat.setVisibility(View.INVISIBLE);
//                    Intent intent=new Intent(activity,InviteEarn.class);
//                    activity.startActivity(intent);
//                    activity.finish();
                }else if(position == 3){
                   // stat.setVisibility(View.INVISIBLE);
//                    Intent intent=new Intent(activity,MoneyActivity.class);
//                    activity.startActivity(intent);
//                    activity.finish();
                }

            }
        });

        return convertView;
    }
//    public void logout(){
//        progressDialog.setMessage("Loading...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
//        url="http://52.35.22.61/shopping/web/logout.php?uid="+preferences.getString("uid","");
//        Log.d("url1111:", "url" + url);
//        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>(){
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                Log.d("res:", "resss" + jsonObject.toString());
//                try {
//                    String Status = jsonObject.getString("status");
//                    if(Status.equalsIgnoreCase("ok")) {
//                        editor.putString("uid","");
//                        editor.putString("reg_id","");
//                        editor.commit();
//                        Intent intent=new Intent(activity,Login.class);
//                        activity.startActivity(intent);
//                        activity.finish();
//////                        JSONObject response=jsonObject.getJSONObject("response");
////                        String user_id = jsonObject.getString("uid");
//////                        String username = response.getString("username");
//////                        String email = response.getString("email");
//////                        editor.putString("uid",user_id);
////                        editor.commit();
//////                        editor.putString("password",_pass);
//////                        Intent intent = new Intent(Login.this, Homeactivity.class);
//////
//////                        startActivity(intent);
//////                        finish();
//////                        Toast.makeText(Login.this, "Login successfully", Toast.LENGTH_SHORT).show();
//////                        Toast.makeText(Login.this, "Invalid username and password", Toast.LENGTH_SHORT).show();
//                    }
////                    else{
//////                        Toast.makeText(Login.this, "Invalid username and password", Toast.LENGTH_SHORT).show();
////
////                    }
//
////                        Toast.makeText(Login.this, "Login successfully", Toast.LENGTH_SHORT).show();
//
//                }catch (Exception e){
//                    Log.d("eeeee:", "eeeee" + e);
//                }
//                progressDialog.dismiss();
//            }
//
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                Log.d("volleyError:", "volleyError" + volleyError);
//            }
//
//
//        });
//        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "tag_json");
//    }
}
